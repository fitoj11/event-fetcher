package main

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"time"

	"albion-event-fetcher/internal/models"
	"albion-event-fetcher/internal/persister"
	"github.com/getsentry/sentry-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	kafka "github.com/segmentio/kafka-go"
)

const KAFKA_TOPIC string = "raw-events"
const KAFKA_READER_GROUP string = "event-processor-group"

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Warning: Failed to load .env file")
	}

	initSentry()

	p := persister.Open(
		os.Getenv("ALBION_KILLS_API_DB"),
		nil,
	)
	p.AddHandler(persister.TwitchVodHandler(
		os.Getenv("TWITCH_CLIENT_ID"),
		os.Getenv("TWITCH_CLIENT_SECRET"),
	))
	p.AddHandler(persister.EloHandler())
	p.AddHandler(persister.FavWeaponHandler())

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{os.Getenv("KAFKA_HOST")},
		GroupID:  KAFKA_READER_GROUP,
		Topic:    KAFKA_TOPIC,
		MaxBytes: 1e6, // 10MB
	})

	ctx := context.Background()
	for {
		m, err := r.FetchMessage(ctx)
		if err != nil {
			log.Println(err)
			sentry.CaptureException(err)
			continue
		}

		var events []*models.Event
		err = json.Unmarshal(m.Value, &events)

		if err != nil {
			log.Println(err)
			sentry.CaptureException(errors.Errorf(
				"Failed to parse json from message %s/%d/%s: %w",
				m.Topic,
				m.Partition,
				string(m.Key),
				err,
			))
			// Since parsing the message failed, we will commit it and move on
			err = r.CommitMessages(ctx, m)
			if err != nil {
				sentry.CaptureException(err)
			}
			continue
		}

		saveCount := 0
		for {
			saveCount, err = p.SaveEvents(events)
			if err != nil {
				err = errors.Errorf(
					"Failed to persist events from %s/%d/%s to DB: %w",
					m.Topic,
					m.Partition,
					string(m.Key),
					err,
				)
				log.Println(err)
				sentry.CaptureException(err)
				log.Println("Waiting 30 seconds to retry")
				time.Sleep(time.Second * 30)
				continue
			}

			break
		}

		log.Printf("Persisted %d events from %s/%d/%s\n", saveCount, m.Topic, m.Partition, string(m.Key))

		err = r.CommitMessages(ctx, m)
		if err != nil {
			sentry.CaptureException(err)
		}
	}
}

func initSentry() {
	if os.Getenv("SENTRY_DSN") != "" {
		log.Println("initializing sentry.io")
		// SENTRY_DSN and SENTRY_ENVIRONMENT must be defined in .env
		if err := sentry.Init(sentry.ClientOptions{}); err != nil {
			log.Printf("Sentry initialization failed: %v\n", err)
		}
	}
}
