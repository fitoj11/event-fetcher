package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"albion-event-fetcher/internal/models"
	"github.com/getsentry/sentry-go"
	lru "github.com/hashicorp/golang-lru"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	kafka "github.com/segmentio/kafka-go"
)

const PAGE_SIZE = 50
const MAX_OFFSET = 1000
const TIMEOUT = 60
const SATURATION_THRESHOLD = 0.90
const EXTENDED_FETCH_THRESHOLD = 0.75
const API_BASE = "https://gameinfo.albiononline.com/api/gameinfo"
const KAFKA_TOPIC string = "raw-events"

type SyncRatio struct {
	total int
	novel int
	sync.Mutex
}

func (c *SyncRatio) Add(total, novel int) {
	c.Lock()
	c.total += total
	c.novel += novel
	c.Unlock()
}

func (c *SyncRatio) Merge(ratio *SyncRatio) {
	c.Lock()
	ratio.Lock()

	c.total += ratio.total
	c.novel += ratio.novel

	ratio.Unlock()
	c.Unlock()
}

func (c *SyncRatio) Reset() {
	c.Lock()
	c.total = 0
	c.novel = 0
	c.Unlock()
}

func (c *SyncRatio) Ratio() float64 {
	c.Lock()
	defer c.Unlock()

	return float64(c.novel) / float64(c.total)
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Warning: Failed to load .env file")
	}

	initSentry()

	log.Printf("Kafka host: %s", os.Getenv("KAFKA_HOST"))
	kafkaWriter := &kafka.Writer{
		Addr:        kafka.TCP(os.Getenv("KAFKA_HOST")),
		Topic:       KAFKA_TOPIC,
		Compression: kafka.Snappy,
	}
	defer kafkaWriter.Close()

	recentEventIds, err := lru.New(10000)
	if err != nil {
		log.Fatalf("Failed to initialize LRU cache for recent event IDs: %v\n", err)
	}

	errorChan := make(chan error, 21)
	go errorWatcher(errorChan)

	for {
		log.Println("Starting fetch cycle")
		novelRatio := fetchToOffset(0, PAGE_SIZE*3, kafkaWriter, errorChan, recentEventIds)
		if novelRatio.Ratio() >= EXTENDED_FETCH_THRESHOLD {
			log.Println("Initial fetch saturation exceeded threshold. Proceeding to an extended fetch")
			extendedNovelRatio := fetchToOffset(PAGE_SIZE*4-1, MAX_OFFSET, kafkaWriter, errorChan, recentEventIds)
			novelRatio.Merge(extendedNovelRatio)
		}

		// Wait for LRU to fill before we start logging saturation
		if recentEventIds.Len() > 2000 {
			logReventEventSaturation(novelRatio.Ratio())
		}
		log.Println(fmt.Sprintf("Waiting %d seconds...", TIMEOUT))
		time.Sleep(time.Second * TIMEOUT)
	}
}

func fetchToOffset(
	initialSkip int,
	maxOffset int,
	kafkaWriter *kafka.Writer,
	errorChan chan<- error,
	recentEventIds *lru.Cache,
) *SyncRatio {
	novelRatio := &SyncRatio{}
	wg := &sync.WaitGroup{}
	for skip := initialSkip; skip <= maxOffset; skip += PAGE_SIZE {
		wg.Add(1)
		go fetchAndQueueEvents(skip, PAGE_SIZE, kafkaWriter, errorChan, wg, recentEventIds, novelRatio)
		time.Sleep(time.Millisecond * 200)
	}
	wg.Wait()

	return novelRatio
}

func logReventEventSaturation(saturation float64) {
	log.Printf("fetch cycle saturation: %0.2f%%", saturation*100)
	if saturation > SATURATION_THRESHOLD {
		sentry.CaptureException(fmt.Errorf("Albion Recent Events API reach %0.2f saturation. Some events might have been missed", saturation))
	}
}

func initSentry() {
	if os.Getenv("SENTRY_DSN") != "" {
		log.Println("initializing sentry.io")
		// SENTRY_DSN and SENTRY_ENVIRONMENT must be defined in .env
		if err := sentry.Init(sentry.ClientOptions{}); err != nil {
			log.Printf("Sentry initialization failed: %v", err)
		}
	}
}

func fetchAndQueueEvents(
	skip,
	take int,
	kafkaWriter *kafka.Writer,
	errorChan chan<- error,
	wg *sync.WaitGroup,
	recentEventIds *lru.Cache,
	novelRatio *SyncRatio,
) {
	defer wg.Done()
	key := fmt.Sprintf("%d-%d-%d", time.Now().Unix(), skip, take)
	log.Printf("Fetching events for %s", key)
	events, err := fetchEvents(skip, take)
	if err != nil {
		errorChan <- err
		return
	}

	novelEvents := make([]*models.Event, 0)
	for _, event := range events {
		if contains, _ := recentEventIds.ContainsOrAdd(event.EventID, true); !contains {
			novelEvents = append(novelEvents, event)
		}
	}

	novelRatio.Add(len(events), len(novelEvents))

	if len(novelEvents) <= 0 {
		return
	}

	eventJson, err := json.Marshal(novelEvents)

	if err != nil {
		err = errors.Errorf(
			"Fetcher failed to encode events from %s to JSON for queueing. %w",
			key,
			err,
		)
		log.Println(err)
		sentry.CaptureException(err)
		return
	}

	err = kafkaWriter.WriteMessages(context.Background(), kafka.Message{
		Key:   []byte(key),
		Value: eventJson,
	})

	if err != nil {
		errorChan <- err
		return
	}

	log.Printf("Queued %d events for %s", len(novelEvents), key)
}

func errorWatcher(errorChan <-chan error) {
	ticker := time.NewTicker(time.Minute * 30)
	defer ticker.Stop()

	const errorThreshold = 300
	errorCount := 0
	for {
		select {
		case err := <-errorChan:
			log.Printf("Failed to fetch events: %v", err)
			errorCount = errorCount + 1
			if errorCount > errorThreshold {
				sentry.CaptureException(fmt.Errorf("Albion API Call error rate has exceeded 50%% in the last 30 minutes"))
			}
		case <-ticker.C:
			errorCount = 0
		}
	}
}

func fetchEvents(skip, take int) ([]*models.Event, error) {
	url := fmt.Sprintf("%s/events?limit=%d&offset=%d&_=%d", API_BASE, take, skip, time.Now().Unix())
	response, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	if response.StatusCode >= 400 {
		return nil, fmt.Errorf("Recieved %d status code while fetching events\n", response.StatusCode)
	}

	var events []*models.Event
	d := json.NewDecoder(response.Body)
	err = d.Decode(&events)

	if err != nil {
		return nil, err
	}

	return events, nil
}
