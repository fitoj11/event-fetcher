package twitchvod

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	TimestampShift = -15 * time.Second
)

type VodFetcher struct {
	clientId             string
	clientSecret         string
	clientToken          string
	clientTokenFetchedOn time.Time
}

func New(clientId, clientSecret string) *VodFetcher {
	return &VodFetcher{
		clientId:     clientId,
		clientSecret: clientSecret,
	}
}

func (f *VodFetcher) FetchTwitchId(name string) (int, error) {
	err := f.tryUpdateToken()
	if err != nil {
		return 0, err
	}

	url := fmt.Sprintf("https://api.twitch.tv/helix/users?login=%s", name)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", f.clientToken))
	request.Header.Set("Client-Id", f.clientId)
	resp, err := http.DefaultClient.Do(request)

	if err != nil {
		return 0, fmt.Errorf("Failed to fetch twitch user: %v\n", err)
	}

	if resp.StatusCode >= 400 {
		return 0, fmt.Errorf("Got http status %d while fetching twitch user\n", resp.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return 0, fmt.Errorf("Failed while reading twich user response: %v\n", err)
	}

	var user TwitchUserResponse
	err = json.Unmarshal(responseBytes, &user)

	if err != nil {
		return 0, fmt.Errorf("Failed to parse twitch user response: %v\n", err)
	}

	if len(user.Data) == 0 {
		return 0, fmt.Errorf("No response found for twitch user: %v\n", err)
	}

	id, err := strconv.Atoi(user.Data[0].ID)
	if err != nil {
		return 0, fmt.Errorf("Failed to parse twitch user id as integer: %v\n", err)
	}

	return id, nil
}

func (f *VodFetcher) FetchVod(userId int, eventTime time.Time) (string, error) {
	err := f.tryUpdateToken()
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf(
		"https://api.twitch.tv/helix/videos?user_id=%d&first=1&type=archive",
		userId,
	)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", fmt.Errorf("Failed to create request for twich vod: %v\n", err)
	}
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", f.clientToken))
	request.Header.Set("Client-Id", f.clientId)
	response, err := http.DefaultClient.Do(request)

	if err != nil {
		return "", fmt.Errorf("Failed to fetch twitch vod: %v\n", err)
	}

	if response.StatusCode >= 400 {
		return "", fmt.Errorf("Got http status %d while fetching twitch vod\n", response.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return "", fmt.Errorf("Failed while reading twich vod response: %v\n", err)
	}

	var vodResponse TwitchVodResponse
	err = json.Unmarshal(responseBytes, &vodResponse)
	if err != nil {
		return "", fmt.Errorf("Failed to parse twitch vod response: %v\n", err)
	}

	if len(vodResponse.Data) == 0 {
		return "", nil
	}

	vod := vodResponse.Data[0]

	if shouldCaptureVod(vod, eventTime) {
		offset := eventTime.Sub(vod.CreatedAt) + TimestampShift
		link := vod.URL + "?t=" + offset.Round(time.Second).String()
		return link, nil
	}

	return "", nil
}

func shouldCaptureVod(vod TwitchVod, eventTime time.Time) bool {
	duration, err := time.ParseDuration(vod.Duration)
	if err != nil {
		return false
	}

	vodEndWithThreshold := vod.CreatedAt.Add(duration + 5*time.Minute)

	return eventTime.Before(vodEndWithThreshold) && eventTime.After(vod.CreatedAt)
}

func (f *VodFetcher) tryUpdateToken() error {
	if time.Now().Sub(f.clientTokenFetchedOn) > 24*time.Hour {
		log.Printf("Fetching new twitch token\n")
		return f.updateToken()
	}

	return nil
}

func (f *VodFetcher) updateToken() error {
	url := fmt.Sprintf("https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials", f.clientId, f.clientSecret)
	resp, err := http.Post(url, "", nil)

	if err != nil {
		return fmt.Errorf("Failed to fetch twitch token: %v\n", err)
	}

	if resp.StatusCode >= 400 {
		return fmt.Errorf("Got http status %d while fetching twitch client token\n", resp.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return fmt.Errorf("Failed while reading twich token response: %v\n", err)
	}

	var token TwitchToken
	err = json.Unmarshal(responseBytes, &token)

	if err != nil {
		return fmt.Errorf("Failed to parse twitch token response: %v\n", err)
	}

	f.clientToken = token.AccessToken
	f.clientTokenFetchedOn = time.Now()

	return nil
}
