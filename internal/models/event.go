package models

import (
	"time"
)

type Event struct {
	EventID              int                `json:"EventId"`
	Timestamp            time.Time          `json:"TimeStamp"`
	Version              int                `json:"Version"`
	TotalVictimKillFame  int                `json:"TotalVictimKillFame"`
	Location             interface{}        `json:"Location"`
	GroupMemberCount     int                `json:"groupMemberCount"`
	NumberOfParticipants int                `json:"numberOfParticipants"`
	Killer               EventPlayer        `json:"Killer"`
	Victim               EventPlayer        `json:"Victim"`
	Participants         []EventParticipant `json:"Participants"`
	GroupMembers         []EventPlayer      `json:"GroupMembers"`
	GvGMatch             interface{}        `json:"GvGMatch"`
	BattleID             int                `json:"BattleId"`
	KillArea             string             `json:"KillArea"`
	Type                 string             `json:"Type"`
}

type EventPlayer struct {
	AverageItemPower float64         `json:"AverageItemPower"`
	Equipment        PlayerEquipment `json:"Equipment"`
	Inventory        []interface{}   `json:"Inventory"`
	Name             string          `json:"Name"`
	ID               string          `json:"Id"`
	GuildName        string          `json:"GuildName"`
	GuildID          string          `json:"GuildId"`
	AllianceName     string          `json:"AllianceName"`
	AllianceID       string          `json:"AllianceId"`
	AllianceTag      string          `json:"AllianceTag"`
	Avatar           string          `json:"Avatar"`
	AvatarRing       string          `json:"AvatarRing"`
	DeathFame        int             `json:"DeathFame"`
	KillFame         int             `json:"KillFame"`
	FameRatio        float64         `json:"FameRatio"`
}

type EventParticipant struct {
	EventPlayer
	DamageDone         float64 `json:"DamageDone"`
	SupportHealingDone float64 `json:"SupportHealingDone"`
}

type PlayerEquipment struct {
	MainHand Equipment `json:"MainHand"`
	OffHand  Equipment `json:"OffHand"`
	Head     Equipment `json:"Head"`
	Armor    Equipment `json:"Armor"`
	Shoes    Equipment `json:"Shoes"`
	Bag      Equipment `json:"Bag"`
	Cape     Equipment `json:"Cape"`
	Mount    Equipment `json:"Mount"`
	Potion   Equipment `json:"Potion"`
	Food     Equipment `json:"Food"`
}

type Equipment struct {
	Type          string        `json:"Type"`
	Count         int           `json:"Count"`
	Quality       int           `json:"Quality"`
	ActiveSpells  []interface{} `json:"ActiveSpells"`
	PassiveSpells []interface{} `json:"PassiveSpells"`
}
