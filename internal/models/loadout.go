package models

import (
	"crypto/sha256"
	"encoding/binary"
	"strconv"
	"strings"
)

type Loadout struct {
	Id              []byte
	MainHandItem    string
	MainHandTier    uint8
	MainHandEnchant uint8
	MainHandQuality uint8
	OffHandItem     string
	OffHandTier     uint8
	OffHandEnchant  uint8
	OffHandQuality  uint8
	HeadItem        string
	HeadTier        uint8
	HeadEnchant     uint8
	HeadQuality     uint8
	BodyItem        string
	BodyTier        uint8
	BodyEnchant     uint8
	BodyQuality     uint8
	ShoeItem        string
	ShoeTier        uint8
	ShoeEnchant     uint8
	ShoeQuality     uint8
	BagItem         string
	BagTier         uint8
	BagEnchant      uint8
	BagQuality      uint8
	CapeItem        string
	CapeTier        uint8
	CapeEnchant     uint8
	CapeQuality     uint8
	MountItem       string
	MountTier       uint8
	MountQuality    uint8
	FoodItem        string
	FoodTier        uint8
	FoodEnchant     uint8
	PotionItem      string
	PotionTier      uint8
	PotionEnchant   uint8
}

func (l *Loadout) SetMainHand(e Equipment) {
	if e.Type == "" {
		return
	}

	l.MainHandTier = parseTierFromType(e.Type)
	l.MainHandItem = parseItemFromType(e.Type, l.MainHandTier)
	l.MainHandEnchant = parseEnchantFromType(e.Type)
	l.MainHandQuality = uint8(e.Quality)
}

func (l *Loadout) SetOffHand(e Equipment) {
	if e.Type == "" {
		return
	}

	l.OffHandTier = parseTierFromType(e.Type)
	l.OffHandItem = parseItemFromType(e.Type, l.OffHandTier)
	l.OffHandEnchant = parseEnchantFromType(e.Type)
	l.OffHandQuality = uint8(e.Quality)
}

func (l *Loadout) SetHead(e Equipment) {
	if e.Type == "" {
		return
	}

	l.HeadTier = parseTierFromType(e.Type)
	l.HeadItem = parseItemFromType(e.Type, l.HeadTier)
	l.HeadEnchant = parseEnchantFromType(e.Type)
	l.HeadQuality = uint8(e.Quality)
}

func (l *Loadout) SetBody(e Equipment) {
	if e.Type == "" {
		return
	}

	l.BodyTier = parseTierFromType(e.Type)
	l.BodyItem = parseItemFromType(e.Type, l.BodyTier)
	l.BodyEnchant = parseEnchantFromType(e.Type)
	l.BodyQuality = uint8(e.Quality)
}

func (l *Loadout) SetShoe(e Equipment) {
	if e.Type == "" {
		return
	}

	l.ShoeTier = parseTierFromType(e.Type)
	l.ShoeItem = parseItemFromType(e.Type, l.ShoeTier)
	l.ShoeEnchant = parseEnchantFromType(e.Type)
	l.ShoeQuality = uint8(e.Quality)
}

func (l *Loadout) SetBag(e Equipment) {
	if e.Type == "" {
		return
	}

	l.BagTier = parseTierFromType(e.Type)
	l.BagItem = parseItemFromType(e.Type, l.BagTier)
	l.BagEnchant = parseEnchantFromType(e.Type)
	l.BagQuality = uint8(e.Quality)
}

func (l *Loadout) SetCape(e Equipment) {
	if e.Type == "" {
		return
	}

	l.CapeTier = parseTierFromType(e.Type)
	l.CapeItem = parseItemFromType(e.Type, l.CapeTier)
	l.CapeEnchant = parseEnchantFromType(e.Type)
	l.CapeQuality = uint8(e.Quality)
}

func (l *Loadout) SetMount(e Equipment) {
	if e.Type == "" {
		return
	}

	l.MountTier = parseTierFromType(e.Type)
	l.MountItem = parseItemFromType(e.Type, l.MountTier)
	l.MountQuality = uint8(e.Quality)
}

func (l *Loadout) SetFood(e Equipment) {
	if e.Type == "" {
		return
	}

	l.FoodTier = parseTierFromType(e.Type)
	l.FoodItem = parseItemFromType(e.Type, l.FoodTier)
	l.FoodEnchant = parseEnchantFromType(e.Type)
}

func (l *Loadout) SetPotion(e Equipment) {
	if e.Type == "" {
		return
	}

	l.PotionTier = parseTierFromType(e.Type)
	l.PotionItem = parseItemFromType(e.Type, l.PotionTier)
	l.PotionEnchant = parseEnchantFromType(e.Type)
}

func (l *Loadout) GenerateId() {
	h := sha256.New()

	h.Write([]byte(l.MainHandItem))
	binary.Write(h, binary.LittleEndian, l.MainHandTier)
	binary.Write(h, binary.LittleEndian, l.MainHandEnchant)
	binary.Write(h, binary.LittleEndian, l.MainHandQuality)

	h.Write([]byte(l.OffHandItem))
	binary.Write(h, binary.LittleEndian, l.OffHandTier)
	binary.Write(h, binary.LittleEndian, l.OffHandEnchant)
	binary.Write(h, binary.LittleEndian, l.OffHandQuality)

	h.Write([]byte(l.HeadItem))
	binary.Write(h, binary.LittleEndian, l.HeadTier)
	binary.Write(h, binary.LittleEndian, l.HeadEnchant)
	binary.Write(h, binary.LittleEndian, l.HeadQuality)

	h.Write([]byte(l.BodyItem))
	binary.Write(h, binary.LittleEndian, l.BodyTier)
	binary.Write(h, binary.LittleEndian, l.BodyEnchant)
	binary.Write(h, binary.LittleEndian, l.BodyQuality)

	h.Write([]byte(l.ShoeItem))
	binary.Write(h, binary.LittleEndian, l.ShoeTier)
	binary.Write(h, binary.LittleEndian, l.ShoeEnchant)
	binary.Write(h, binary.LittleEndian, l.ShoeQuality)

	h.Write([]byte(l.BagItem))
	binary.Write(h, binary.LittleEndian, l.BagTier)
	binary.Write(h, binary.LittleEndian, l.BagEnchant)
	binary.Write(h, binary.LittleEndian, l.BagQuality)

	h.Write([]byte(l.CapeItem))
	binary.Write(h, binary.LittleEndian, l.CapeTier)
	binary.Write(h, binary.LittleEndian, l.CapeEnchant)
	binary.Write(h, binary.LittleEndian, l.CapeQuality)

	h.Write([]byte(l.MountItem))
	binary.Write(h, binary.LittleEndian, l.MountTier)
	binary.Write(h, binary.LittleEndian, l.MountQuality)

	h.Write([]byte(l.FoodItem))
	binary.Write(h, binary.LittleEndian, l.FoodTier)
	binary.Write(h, binary.LittleEndian, l.FoodEnchant)

	h.Write([]byte(l.PotionItem))
	binary.Write(h, binary.LittleEndian, l.PotionTier)
	binary.Write(h, binary.LittleEndian, l.PotionEnchant)

	l.Id = h.Sum(make([]byte, 0))
}

func parseTierFromType(t string) uint8 {
	parts := strings.SplitN(t, "_", 2)

	switch parts[0] {
	case "T1":
		return 1
	case "T2":
		return 2
	case "T3":
		return 3
	case "T4":
		return 4
	case "T5":
		return 5
	case "T6":
		return 6
	case "T7":
		return 7
	case "T8":
		return 8
	default:
		return 0
	}
}

func parseEnchantFromType(t string) uint8 {
	parts := strings.Split(t, "@")

	if len(parts) == 2 {
		result, err := strconv.Atoi(parts[1])
		if err != nil {
			return 0
		}

		return uint8(result)
	}

	return 0
}

func parseItemFromType(t string, tier uint8) string {
	if t == "" {
		return ""
	}

	nameWithTier := strings.Split(t, "@")[0]

	if tier == 0 {
		return nameWithTier
	}

	parts := strings.SplitN(nameWithTier, "_", 2)

	if len(parts) > 1 {
		return parts[1]
	}

	return parts[0]
}
