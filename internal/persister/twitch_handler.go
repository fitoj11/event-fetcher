package persister

import (
	"albion-event-fetcher/internal/models"
	"albion-event-fetcher/internal/twitchvod"
	"github.com/getsentry/sentry-go"
	"github.com/jmoiron/sqlx"
	"log"
	"strings"
	"time"
)

const (
	TwitchUserSyncInterval = 5 * time.Minute
)

type twitchHandler struct {
	db          *sqlx.DB
	twitchUsers map[string]int
	vodFetcher  *twitchvod.VodFetcher
}

func TwitchVodHandler(twitchClientId, twitchClientSecret string) *twitchHandler {
	handler := &twitchHandler{
		vodFetcher: twitchvod.New(twitchClientId, twitchClientSecret),
	}

	return handler
}

func (h *twitchHandler) handleEvents(events []*models.Event) {
	for _, event := range events {
		h.handleEvent(event)
	}
}

func (h *twitchHandler) setup(db *sqlx.DB) {
	h.db = db
	h.startTwitchUserLoader()
}

func (h *twitchHandler) handleEvent(event *models.Event) {
	if userId, ok := h.twitchUsers[strings.ToLower(event.Killer.Name)]; ok {
		go h.saveTwitchVod(event.Killer.Name, userId, event)
	}
	if userId, ok := h.twitchUsers[strings.ToLower(event.Victim.Name)]; ok {
		go h.saveTwitchVod(event.Victim.Name, userId, event)
	}
}

func (h *twitchHandler) saveTwitchVod(name string, twitchUserId int, event *models.Event) {
	link, err := h.vodFetcher.FetchVod(twitchUserId, event.Timestamp)
	if err != nil {
		sentry.CaptureException(err)
		return
	}

	if link == "" {
		return
	}

	_, err = h.db.Exec(
		`
insert into twitch_vods (
event_id, name, link
) VALUES (
?, ?, ?
)
    `, event.EventID, name, link)

	if err != nil {
		log.Printf("Failed to save twich vod: %v\n", err)
	}
}

func (h *twitchHandler) startTwitchUserLoader() {
	err := h.loadTwitchUsers()
	if err != nil {
		sentry.CaptureException(err)
	}

	ticker := time.NewTicker(TwitchUserSyncInterval)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := h.loadTwitchUsers()
				if err != nil {
					sentry.CaptureException(err)
				}
			}
		}
	}()
}

func (h *twitchHandler) loadTwitchUsers() error {
	rows, err := h.db.Query("select name, twitch_name, twitch_user_id from twitch_users")
	if err != nil {
		return err
	}
	defer rows.Close()

	h.twitchUsers = make(map[string]int)
	for rows.Next() {
		var name string
		var twitch_id int
		var twitch_name string
		err = rows.Scan(&name, &twitch_name, &twitch_id)

		if err != nil {
			return err
		}

		if twitch_id == 0 {
			twitch_id, err = h.vodFetcher.FetchTwitchId(twitch_name)
			if err != nil {
				log.Printf("failed to fetch twitch ID for user: %s: %v\n", twitch_name, err)
				continue
			}

			_, err = h.db.Exec("update twitch_users set twitch_user_id = ? where name = ?", twitch_id, name)
			if err != nil {
				return err
			}
		}

		h.twitchUsers[strings.ToLower(name)] = twitch_id
	}
	log.Printf("Loaded twitch users: %v\n", h.twitchUsers)

	return nil
}
