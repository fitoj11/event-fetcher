package persister

import (
	"database/sql"
	"fmt"
	"log"

	"albion-event-fetcher/internal/elo"
	"albion-event-fetcher/internal/models"
	"github.com/jmoiron/sqlx"

	"github.com/getsentry/sentry-go"
)

const (
	initialElo = 1000
)

type eloHandler struct {
	db *sqlx.DB
}

func EloHandler() *eloHandler {
	handler := &eloHandler{}

	return handler
}

func (h *eloHandler) setup(db *sqlx.DB) {
	h.db = db
}

func (h *eloHandler) handleEvents(events []*models.Event) {
	for _, event := range events {
		h.handleEvent(event)
	}
}

func (h *eloHandler) handleEvent(event *models.Event) {
	isSlayer := is1v1SlayerEvent(event)

	if !is1v1StalkerEvent(event) && !isSlayer {
		return
	}

	winnerRating, err := h.getPlayerRating(event.Killer.Name, isSlayer)
	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to fetch winner elo for %s: %v\n", event.Killer.Name, err))
		return
	}
	loserRating, err := h.getPlayerRating(event.Victim.Name, isSlayer)
	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to fetch loser elo for %s: %v\n", event.Victim.Name, err))
		return
	}

	winnerPts, loserPts := elo.CalcPoints(
		winnerRating,
		loserRating,
		int(event.Killer.AverageItemPower),
		int(event.Victim.AverageItemPower),
	)

	err = h.updateElo(
		event.Killer.Name,
		event.Victim.Name,
		winnerRating,
		loserRating,
		winnerPts,
		loserPts,
		event.EventID,
		isSlayer,
	)

	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to write elo transaction for event %d: %v\n", event.EventID, err))
	}
}

func (h *eloHandler) getPlayerRating(name string, isSlayer bool) (int, error) {
	tableName := getEloTable(isSlayer)
	query := fmt.Sprintf("select rating from %s where name = ?", tableName)
	row := h.db.QueryRow(query, name)
	var rating int
	err := row.Scan(&rating)
	if err == sql.ErrNoRows {
		return initialElo, nil
	} else if err != nil {
		return 0, err
	}

	return rating, nil
}

func (h *eloHandler) updateElo(
	winnerName,
	loserName string,
	winnerRating,
	loserRating,
	winnerPts,
	loserPts,
	eventId int,
	isSlayer bool,
) error {
	tx, err := h.db.Begin()
	if err != nil {
		return err
	}

	newWinnerRating := winnerRating + winnerPts
	newLoserRating := loserRating - loserPts

	tableName := getEloTable(isSlayer)

	_, err = tx.Exec(fmt.Sprintf(
		"INSERT INTO %s (name, rating) VALUES (?, ?) ON DUPLICATE KEY UPDATE rating = ?, last_update= now()",
		tableName,
	), winnerName, newWinnerRating, newWinnerRating)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(fmt.Sprintf(
		"INSERT INTO %s (name, rating) VALUES (?, ?) ON DUPLICATE KEY UPDATE rating = ?, last_update = now()",
		tableName,
	), loserName, newLoserRating, newLoserRating)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(
		"INSERT INTO elo_transaction_1v1 (winner_name, loser_name, event_id, points_awarded, points_lost, is_slayer) VALUES (?, ?, ?, ?, ?, ?)",
		winnerName,
		loserName,
		eventId,
		winnerPts,
		loserPts,
		isSlayer,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func getEloTable(isSlayer bool) string {
	if isSlayer {
		return "elo_slayer_1v1"
	}

	return "elo_stalker_1v1"
}

func is1v1StalkerEvent(event *models.Event) bool {
	isStalker := event.Killer.AverageItemPower > 900 && event.Killer.AverageItemPower < 1100 && event.Victim.AverageItemPower > 900 && event.Victim.AverageItemPower < 1100

	return event.GroupMemberCount == 1 && event.NumberOfParticipants == 1 && isStalker
}

func is1v1SlayerEvent(event *models.Event) bool {
	isSlayer := event.Killer.AverageItemPower > 1180 && event.Victim.AverageItemPower > 1180

	return event.GroupMemberCount == 1 && event.NumberOfParticipants == 1 && isSlayer
}
