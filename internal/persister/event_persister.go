package persister

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"albion-event-fetcher/internal/models"

	"github.com/getsentry/sentry-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Persister struct {
	db       *sqlx.DB
	tx       *sql.Tx
	handlers []handler
	c        <-chan []*models.Event
}

func Open(connectionString string, c <-chan []*models.Event) *Persister {
	persister := &Persister{
		handlers: make([]handler, 0),
		c:        c,
	}
	db, err := sqlx.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}

	persister.db = db

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	if c != nil {
		go withRestart(persister.eventPump)
	}

	extractor := BattleExtractor2v2(db)
	extractor.Start()

	return persister
}

func withRestart(f func()) {
	defer func() {
		if r := recover(); r != nil {
			sentry.CaptureException(fmt.Errorf("%v", r))
			log.Println("goroutine stopped, will restart", r)
			withRestart(f)
		}
	}()

	f()
}

func (p *Persister) eventPump() {
	log.Println("Starting event pump")
	for events := range p.c {
		newEventCount, err := p.SaveEvents(events)

		if err != nil {
			sentry.CaptureException(err)
			log.Printf("Failed to persist events: %v\n", err)
		} else {
			log.Printf("Inserted %d new events\n", newEventCount)
		}
	}
}

func (p *Persister) AddHandler(h handler) {
	h.setup(p.db)
	p.handlers = append(p.handlers, h)
}

func (p *Persister) SaveEvents(events []*models.Event) (int, error) {
	count, err := p.writeEventsToDb(events)

	if err != nil {
		return 0, err
	}

	return count, err
}

func (p *Persister) writeEventsToDb(events []*models.Event) (int, error) {
	tx, err := p.db.Begin()
	if err != nil {
		return 0, fmt.Errorf("Failed to start transaction: %w", err)
	}
	p.tx = tx

	stmt, err := tx.Prepare("insert ignore into events (event_id, time, version, total_kill_fame, participant_count, party_size, battle_id) values (?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		p.tx.Rollback()
		return 0, fmt.Errorf("Failed to create prepared statement for insterting events: %w", err)
	}
	defer stmt.Close()

	newEvents := make([]*models.Event, 0)

	for _, event := range events {
		result, err := stmt.Exec(
			event.EventID,
			event.Timestamp.Format("2006-01-02 15:04:05"),
			event.Version,
			event.TotalVictimKillFame,
			event.NumberOfParticipants,
			uint8(event.GroupMemberCount),
			event.BattleID,
		)

		if err != nil {
			p.tx.Rollback()
			return 0, fmt.Errorf("Failed to save event with ID %d : %w", event.EventID, err)
		}

		rowsAffected, err := result.RowsAffected()

		if err != nil {
			p.tx.Rollback()
			return 0, fmt.Errorf("Failed to fetch rows affected after inserting event %d: %w", event.EventID, err)
		}

		if rowsAffected > 0 {
			err = p.saveKillers(event)
			if err != nil {
				p.tx.Rollback()
				return 0, fmt.Errorf("Failed to save killers for event %d: %w", event.EventID, err)
			}

			err = p.saveVictim(event)
			if err != nil {
				p.tx.Rollback()
				return 0, fmt.Errorf("Failed to save victim for event %d: %w", event.EventID, err)
			}

			newEvents = append(newEvents, event)
		}
	}

	err = p.tx.Commit()

	if err != nil {
		return 0, err
	}

	p.runHandlers(newEvents)

	return len(newEvents), nil
}

func (p *Persister) runHandlers(newEvents []*models.Event) {
	for _, handler := range p.handlers {
		handler.handleEvents(newEvents)
	}
}

func (p *Persister) saveKillers(event *models.Event) error {
	stmt, err := p.tx.Prepare(
		`insert into killers (
    event_id, is_primary, name, kill_fame, damage_done,
    healing_done, item_power, guild_name, alliance_name, loadout
) VALUES (
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?
)`)

	if err != nil {
		return err
	}

	defer stmt.Close()

	primaryFound := false
	for _, participant := range event.Participants {
		isPrimary := participant.ID == event.Killer.ID
		primaryFound = isPrimary || primaryFound
		killFame := findKillerFame(event, participant.ID)

		lid, err := p.saveEquipment(participant.Equipment)

		if err != nil {
			return err
		}

		_, err = stmt.Exec(
			event.EventID, isPrimary, participant.Name, killFame, uint(participant.DamageDone),
			uint(participant.SupportHealingDone), uint16(participant.AverageItemPower), participant.GuildName, participant.AllianceName, lid.Id,
		)

		if err != nil {
			return err
		}
	}

	if !primaryFound {
		lid, err := p.saveEquipment(event.Killer.Equipment)
		if err != nil {
			return err
		}

		_, err = stmt.Exec(
			event.EventID, 1, event.Killer.Name, event.Killer.KillFame, 0,
			0, uint(event.Killer.AverageItemPower), event.Killer.GuildName, event.Killer.AllianceName, lid.Id,
		)
	}

	return nil
}

func (p *Persister) saveVictim(event *models.Event) error {
	lid, err := p.saveEquipment(event.Victim.Equipment)

	if err != nil {
		return err
	}

	stmt, err := p.tx.Prepare(
		`insert into victims (
    event_id, name, item_power, guild_name,
    alliance_name, loadout
) VALUES (
    ?, ?, ?, ?, ?,
    ?
)`)

	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		event.EventID, event.Victim.Name, uint16(event.Victim.AverageItemPower), event.Victim.GuildName,
		event.Victim.AllianceName, lid.Id,
	)

	if err != nil {
		return err
	}

	return nil
}

func (p *Persister) saveEquipment(eq models.PlayerEquipment) (*models.Loadout, error) {
	l := &models.Loadout{}

	l.SetMainHand(eq.MainHand)
	l.SetOffHand(eq.OffHand)
	l.SetHead(eq.Head)
	l.SetBody(eq.Armor)
	l.SetShoe(eq.Shoes)
	l.SetBag(eq.Bag)
	l.SetCape(eq.Cape)
	l.SetMount(eq.Mount)
	l.SetFood(eq.Food)
	l.SetPotion(eq.Potion)

	l.GenerateId()

	stmt, err := p.tx.Prepare(
		`insert ignore into loadouts (
    id, main_hand_item, main_hand_tier, main_hand_enchant, main_hand_quality,
    off_hand_item, off_hand_tier, off_hand_enchant, off_hand_quality, head_item,
    head_tier, head_enchant, head_quality, body_item, body_tier,
    body_enchant, body_quality, shoe_item, shoe_tier, shoe_enchant,
    shoe_quality, bag_item, bag_tier, bag_enchant, bag_quality,
    cape_item, cape_tier, cape_enchant, cape_quality, mount_item,
    mount_tier, mount_quality, food_item, food_tier, food_enchant,
    potion_item, potion_tier, potion_enchant
) VALUES (
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?,
    ?, ?, ?
)`)

	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		l.Id, l.MainHandItem, l.MainHandTier, l.MainHandEnchant, l.MainHandQuality,
		l.OffHandItem, l.OffHandTier, l.OffHandEnchant, l.OffHandQuality, l.HeadItem,
		l.HeadTier, l.HeadEnchant, l.HeadQuality, l.BodyItem, l.BodyTier,
		l.BodyEnchant, l.BodyQuality, l.ShoeItem, l.ShoeTier, l.ShoeEnchant,
		l.ShoeQuality, l.BagItem, l.BagTier, l.BagEnchant, l.BagQuality,
		l.CapeItem, l.CapeTier, l.CapeEnchant, l.CapeQuality, l.MountItem,
		l.MountTier, l.MountQuality, l.FoodItem, l.FoodTier, l.FoodEnchant,
		l.PotionItem, l.PotionTier, l.PotionEnchant,
	)

	if err != nil {
		return nil, err
	}

	return l, nil
}

func findKillerFame(event *models.Event, playerId string) int {
	for _, player := range event.GroupMembers {
		if player.ID == playerId {
			return player.KillFame
		}
	}

	return 0
}
