package rnkr

import (
	"fmt"
)

const INITIAL_SKILL = 25.0
const INITIAL_UNCERTAINTY = 25.0 / 3.0

type Rating struct {
	Label              string
	EstimatedSkill     float64
	Uncertainty        float64
	UncertaintySquared float64
}

func NewRating(label string, skill, uncertainty float64) *Rating {
	return &Rating{
		Label:              label,
		EstimatedSkill:     skill,
		Uncertainty:        uncertainty,
		UncertaintySquared: uncertainty * uncertainty,
	}
}

func DefaultRating() *Rating {
	return &Rating{
		EstimatedSkill:     INITIAL_SKILL,
		Uncertainty:        INITIAL_UNCERTAINTY,
		UncertaintySquared: INITIAL_UNCERTAINTY * INITIAL_UNCERTAINTY,
	}
}

func (r *Rating) Rating() float64 {
	return r.EstimatedSkill - 3.0*r.Uncertainty
}

func (r *Rating) String() string {
	if r.Label == "" {
		return fmt.Sprintf("%3f (%3f, %3f)", r.Rating(), r.EstimatedSkill, r.Uncertainty)
	}

	return fmt.Sprintf("%s: %3f (%3f, %3f)", r.Label, r.Rating(), r.EstimatedSkill, r.Uncertainty)
}
