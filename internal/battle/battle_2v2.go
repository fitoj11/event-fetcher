package battle

import (
	"log"
)

type BattleEvent struct {
	BattleId      int    `db:"battle_id"`
	EventId       int    `db:"event_id"`
	Timestamp     int    `db:"time"`
	KillerName    string `db:"killer_name"`
	VictimName    string `db:"victim_name"`
	KillerLoadout []byte `db:"killer_loadout"`
	VictimLoadout []byte `db:"victim_loadout"`
	KillerIp      int    `db:"killer_ip"`
	VictimIp      int    `db:"victim_ip"`
}

type Battle2v2 struct {
	BattleId       int    `db:"battle_id"`
	Timestamp      int    `db:"time"`
	Winner1        string `db:"winner_1"`
	Winner2        string `db:"winner_2"`
	Loser1         string `db:"loser_1"`
	Loser2         string `db:"loser_2"`
	Winner1Loadout []byte `db:"winner_1_loadout"`
	Winner2Loadout []byte `db:"winner_2_loadout"`
	Loser1Loadout  []byte `db:"loser_1_loadout"`
	Loser2Loadout  []byte `db:"loser_2_loadout"`
	events         []*BattleEvent
}

func (b *Battle2v2) AddEvent(e *BattleEvent) {
	b.events = append(b.events, e)
}

func (b *Battle2v2) Extract() bool {
	players := make(map[string]*battlePlayer)

	for _, event := range b.events {
		b.BattleId = event.BattleId
		b.Timestamp = event.Timestamp

		if _, ok := players[event.VictimName]; !ok {
			players[event.VictimName] = newBattlePlayer(event.VictimName, event.VictimLoadout)
		}
		if _, ok := players[event.KillerName]; !ok {
			players[event.KillerName] = newBattlePlayer(event.KillerName, event.KillerLoadout)
		}
		players[event.VictimName].addKiller(event.KillerName)
	}

	if len(players) < 4 {
		log.Printf("Failed to extract because there were only %d players\n", len(players))
		return false
	}

	if len(players) > 4 {
		log.Printf("Failed to extract because there were %d players\n", len(players))
		return false
	}

	var team1 []*battlePlayer = nil
	var team2 []*battlePlayer = nil
	for _, player := range players {
		if len(player.killers) == 2 {
			team1 = []*battlePlayer{players[player.killers[0]], players[player.killers[1]]}
			team2 = []*battlePlayer{player}
			break
		}
	}

	if team1 == nil {
		log.Printf("Failed to extract teams\n")
		return false
	}

	for name, player := range players {
		if name != team1[0].name && name != team1[1].name && name != team2[0].name {
			team2 = append(team2, player)
			break
		}
	}

	var winners []*battlePlayer = nil
	var losers []*battlePlayer = nil

	if len(team1[0].killers) == 0 || len(team1[1].killers) == 0 {
		winners = team1
		losers = team2
	} else {
		winners = team2
		losers = team1
	}

	b.Winner1 = winners[0].name
	b.Winner2 = winners[1].name
	b.Loser1 = losers[0].name
	b.Loser2 = losers[1].name
	b.Winner1Loadout = winners[0].loadout
	b.Winner2Loadout = winners[1].loadout
	b.Loser1Loadout = losers[0].loadout
	b.Loser2Loadout = losers[1].loadout

	return true
}

type battlePlayer struct {
	name    string
	loadout []byte
	killers []string
}

func newBattlePlayer(name string, loadout []byte) *battlePlayer {
	return &battlePlayer{
		name:    name,
		loadout: loadout,
		killers: make([]string, 0),
	}
}

func (p *battlePlayer) addKiller(killer string) {
	p.killers = append(p.killers, killer)
}
